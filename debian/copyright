Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ycmd
Source: https://github.com/ycm-core/ycmd
Files-Excluded:
 cpp/llvm
 cpp/pybind11
 cpp/whereami
 cpp/ycm/UnicodeTable.inc
 cpp/ycm/tests/GraphemeBreakCases.inc
 cpp/ycm/tests/NormalizationCases.inc
 third_party
 docs/bundle.js*
 docs/main.css*
 docs/index.html

Files: *
Copyright: 2011-2015 Google Inc.,
 2011-2024 ycmd contributors
License: GPL-3+

Files: .ycm_extra_conf.py
Copyright: 2011-2013 Strahinja Val Markovic <val@markovic.io>
Comment: The upstream distribution does not contain an explicit statement of
 copyright ownership. Pursuant to the Berne Convention for the Protection of
 Literary and Artistic Works, it is assumed that all content is copyright by
 its respective authors unless otherwise stated.
License: Unlicense

Files: cpp/ycm/tests/cmake/FindGMock.cmake
Copyright: 2011 Matej Svec
License: MIT

Files: examples/*
Copyright: 2020 ycmd contributors
License: Apache-2.0

Files: examples/samples/*
Copyright: 2014 Google Inc.
License: Apache-2.0

Files: examples/samples/some_vimscript.vim
Copyright: 2020 ycmd contributors
License: Apache-2.0

Files: ycmd/tests/clang/include_cache_test.py
Copyright: 2017 Davit Samvelyan davitsamvelyan@gmail.com,
 2017 Synopsys
 2021 ycmd contributors
License: GPL-3+

Files: ycmd/tests/clang/testdata/cuda/cuda.h
 ycmd/tests/clangd/testdata/cuda/cuda.h
 ycmd/tests/clang/testdata/cuda/kernel_call.cu
 ycmd/tests/clangd/testdata/cuda/kernel_call.cu
 ycmd/tests/clangd/testdata/FixIt_Clang_cpp11.cpp
Copyright: 2007-2016 University of Illinois at Urbana-Champaign
License: LLVM

Files: ycmd/completers/cpp/include_cache.py
Copyright: 2017 Davit Samvelyan davitsamvelyan@gmail.com,
 2017 Synopsys,
 2020 ycmd contributors
License: GPL-3+

Files: ycmd/completers/general/filename_completer.py
 ycmd/completers/general/general_completer_store.py
Copyright: 2013 Stanislav Golovanov <stgolovanov@gmail.com>,
 2013 Google Inc.
License: GPL-3+

Files: debian/*
Copyright: 2013-2017 Onur Aslan <onur@onur.im>
 2018-2024 David Kalnischkies <donkult@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: Unlicense
 This is free and unencumbered software released into the public domain.
 .
 Anyone is free to copy, modify, publish, use, compile, sell, or
 distribute this software, either in source code form or as a compiled
 binary, for any purpose, commercial or non-commercial, and by any
 means.
 .
 In jurisdictions that recognize copyright laws, the author or authors
 of this software dedicate any and all copyright interest in the
 software to the public domain. We make this dedication for the benefit
 of the public at large and to the detriment of our heirs and
 successors. We intend this dedication to be an overt act of
 relinquishment in perpetuity of all present and future rights to this
 software under copyright law.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 .
 For more information, please refer to <http://unlicense.org/>

License: LLVM
 Copyright (c) 2007-2016 University of Illinois at Urbana-Champaign.
 All rights reserved.
 .
 Developed by:
 .
     LLVM Team
 .
     University of Illinois at Urbana-Champaign
 .
     http://llvm.org
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 with the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
     * Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimers.
 .
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimers in the
       documentation and/or other materials provided with the distribution.
 .
     * Neither the names of the LLVM Team, University of Illinois at
       Urbana-Champaign, nor the names of its contributors may be used to
       endorse or promote products derived from this Software without
       specific prior written permission.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH
 THE SOFTWARE.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
Comment:
 On Debian systems, the full text of the Apache 2.0 License
 can be found in the file `/usr/share/common-licenses/Apache-2.0'.
